import { ExpenseDAOPostres } from "../src/daos/expenses-dao-postgres-impl";
import { Expense } from "../src/entities";
import ExpenseService from "./expense-service";

export class ExpenseServiceImpl implements ExpenseService
{
    expenseDAO:ExpenseDAOPostres = new ExpenseDAOPostres();

    registerExpense(expense:Expense): Promise<Expense> 
    {
        return this.expenseDAO.createExpense(expense);
    }

    retrieveAllExpenses(): Promise<Expense[]> 
    {
        return this.expenseDAO.getAllExpenses();
    }

    retrieveExpenseById(id:number): Promise<Expense> 
    {
        return this.expenseDAO.getExpenseById(id);
    }

    modifyExpense(expense: Expense): Promise<Expense> 
    {
        return this.expenseDAO.updateExpense(expense);
    }

    removeExpense(id:number): Promise<Boolean>
    {
        return this.expenseDAO.deleteExpenseById(id);
    }
    
}