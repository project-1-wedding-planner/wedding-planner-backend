// DAO (Data Access Object)
// A class that is responsible for persisting an entity
import { Wedding } from "../entities";

// A DAO should support the CRUD operations
export interface WeddingDAO
{
    createWedding(wedding:Wedding):Promise<Wedding>;
    getAllWeddings():Promise<Wedding[]>;
    getWeddingById(id:number):Promise<Wedding>;
    updateWedding(wedding:Wedding):Promise<Wedding>;
    deleteWeddingById(id:number):Promise<boolean>;
}