// import { readFile, writeFile } from "fs/promises";
import { Wedding } from "../entities";
import { WeddingDAO } from "./wedding-dao";
import { client } from "../connection";
import { MissingResourceError } from "../error";

export class WeddingDAOPostres implements WeddingDAO
{

    async createWedding(wedding: Wedding): Promise<Wedding> 
    {
        const sql:string = "insert into wedding(w_date, w_name, w_location, w_budget) values ($1, $2, $3, $4) returning id";
        const values = [wedding.date, wedding.name, wedding.location, wedding.budget];
        const result = await client.query(sql, values);
        wedding.id = result.rows[0].id;
        return wedding;
    }

    async getAllWeddings(): Promise<Wedding[]> 
    {
        const sql:string = 'select * from wedding';
        const result = await client.query(sql);
        const weddings:Wedding[] = [];
        for(const row of result.rows)
        {
            const wedding:Wedding = new Wedding
            (
                row.id,
                row.w_date,
                row.w_name,
                row.w_location,
                row.w_budget
            );
            weddings.push(wedding);
        }
        return weddings;
    }

    async getWeddingById(id: number): Promise<Wedding> 
    {
        const sql:string = 'select * from wedding where id = $1';
        const values = [id];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The wedding with id ${id} does not exist`);
        }
        const row = result.rows[0];
        const wedding:Wedding = new Wedding
        (
            row.id, 
            row.w_date,
            row.w_name,
            row.w_location,
            row.w_budget
        );
        return wedding;
    }

    async updateWedding(wedding:Wedding): Promise<Wedding> 
    {
        const sql:string = 'update wedding set w_date=$1, w_name=$2, w_location=$3, w_budget=$4 where id=$5';
        const values = [wedding.date, wedding.name, wedding.location, wedding.budget, wedding.id];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The wedding with id ${wedding.id} does not exist`);
        }
        return wedding;
    }

    async deleteWeddingById(id: number): Promise<boolean> 
    {
        const sql:string = 'delete from wedding where id=$1';
        const values = [id];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The wedding with id ${id} does not exist`);
        }
        return true;
    }
}