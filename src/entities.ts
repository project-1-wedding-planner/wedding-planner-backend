
export class Wedding
{
    constructor
    (
        public id:number,
        public date:string,
        public name:string,
        public location:string,
        public budget:number        
    ){};
}

export class Expense
{
    constructor
    (
        public id:number,
        public reason:string,
        public amount:number,
        public wedding_id:number,
        public photo:string      
    ){};
}