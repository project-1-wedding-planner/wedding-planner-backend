"use strict";
// SERVICE LAYER!
// RELIES ON THE DATA LAYER AND PROVIDES "SERVICES" TO THE API LAYER
exports.__esModule = true;
exports.WeddingServiceImpl = void 0;
var wedding_dao_postgres_impl_1 = require("../src/daos/wedding-dao-postgres-impl");
var WeddingServiceImpl = /** @class */ (function () {
    function WeddingServiceImpl() {
        this.weddingDAO = new wedding_dao_postgres_impl_1.WeddingDAOPostres();
    }
    WeddingServiceImpl.prototype.registerWedding = function (wedding) {
        return this.weddingDAO.createWedding(wedding);
    };
    WeddingServiceImpl.prototype.retrieveAllWeddings = function () {
        return this.weddingDAO.getAllWeddings();
    };
    WeddingServiceImpl.prototype.retrieveWeddingById = function (id) {
        return this.weddingDAO.getWeddingById(id);
    };
    WeddingServiceImpl.prototype.modifyWedding = function (wedding) {
        return this.weddingDAO.updateWedding(wedding);
    };
    WeddingServiceImpl.prototype.removeWedding = function (id) {
        return this.weddingDAO.deleteWeddingById(id);
    };
    return WeddingServiceImpl;
}());
exports.WeddingServiceImpl = WeddingServiceImpl;
