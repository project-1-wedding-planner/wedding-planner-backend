"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var cors_1 = __importDefault(require("cors"));
var express_1 = __importDefault(require("express"));
var error_1 = require("./error");
var wedding_service_impl_1 = require("../services/wedding-service-impl");
var expense_service_impl_1 = require("../services/expense-service-impl");
var app = (0, express_1["default"])();
app.use(express_1["default"].json()); // Middleware
app.use((0, cors_1["default"])());
var weddingService = new wedding_service_impl_1.WeddingServiceImpl();
var expenseService = new expense_service_impl_1.ExpenseServiceImpl();
/*
Endpoint: GET all weddings
Description: Get all weddings within our current wedding table: returning the ID, date, name, location, and budget of the wedding.
*/
app.get("/weddings", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var wedding;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, weddingService.retrieveAllWeddings()];
            case 1:
                wedding = _a.sent();
                res.status(200);
                res.send(wedding);
                return [2 /*return*/];
        }
    });
}); });
/*
Endpoint: GET wedding by ID
Description: Gets the wedding with matching ID within our current client table: returning the ID, date, name, location, and budget of the wedding.
             Returns an error if the specified wedding ID does not exist.
*/
app.get("/weddings/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var weddingId, wedding, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                weddingId = Number(req.params.id);
                return [4 /*yield*/, weddingService.retrieveWeddingById(weddingId)];
            case 1:
                wedding = _a.sent();
                res.status(200);
                res.send(wedding);
                return [3 /*break*/, 3];
            case 2:
                error_2 = _a.sent();
                if (error_2 instanceof error_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_2);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
/*
Endpoint: POST a new wedding
Description: Registers a new wedding to our wedding table.  They will be given a default wedding ID and specified date, name, location, and budget.
             Returns an error if more or less than the required field(s) are provided.
*/
app.post("/weddings", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var wedding;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (Object.keys(req.body).length > 4) {
                    throw new error_1.MissingResourceError("message: Cannot add more properties than date, name, location, and budget for the wedding.");
                }
                wedding = req.body;
                return [4 /*yield*/, weddingService.registerWedding(wedding)];
            case 1:
                wedding = _a.sent();
                res.status(201);
                res.send(wedding);
                return [2 /*return*/];
        }
    });
}); });
/*
Endpoint: PUT update wedding by ID
Description: Updates the wedding information in the wedding table. Wedding ID provided must match URL ID.
             Returns an error if there are more than 2 properties or the URL ID does not match the ID of the body.
*/
app.put("/weddings/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, wedding, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 5]);
                if (Object.keys(req.body).length > 5) {
                    throw new error_1.MissingResourceError("message: Cannot add properties other than wedding ID, date, name, location, and budget.");
                }
                id = Number(req.params.id);
                wedding = req.body;
                if (!(id === req.body.id)) return [3 /*break*/, 2];
                return [4 /*yield*/, weddingService.modifyWedding(wedding)];
            case 1:
                wedding = _a.sent();
                res.status(200);
                res.send(wedding);
                return [3 /*break*/, 3];
            case 2:
                res.status(403);
                res.send({ "message": "Error. ID of URL must match body ID." });
                _a.label = 3;
            case 3: return [3 /*break*/, 5];
            case 4:
                error_3 = _a.sent();
                if (error_3 instanceof error_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_3);
                }
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
/*
Endpoint: DELETE wedding
Description: Deletes a wedding from our wedding table with matching wedding ID. Returns an error if the wedding ID
             does not exist.
*/
app["delete"]("/weddings/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, weddingExists, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                id = Number(req.params.id);
                weddingExists = req.body;
                return [4 /*yield*/, weddingService.removeWedding(id)];
            case 1:
                weddingExists = _a.sent();
                res.status(205);
                res.send(weddingExists);
                return [3 /*break*/, 3];
            case 2:
                error_4 = _a.sent();
                if (error_4 instanceof error_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_4);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
// ACCOUNTS ---------------------------------------------------------------------------------------------------------------------
/*
Endpoint: POST new expense for a wedding
Description: Creates a new expense for an existing wedding.  URL specifies the wedding ID and and the expense ID is auto generated.
             The reason and amount fields must be specified.  Returns an error if more than the reason and amount fields are given,
             the wedding ID is invalid, or the amount provided is below zero.
*/
app.post("/weddings/:id/expenses", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, wedding, error_5, expense;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = Number(req.params.id);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 5, , 6]);
                if (!(Object.keys(req.body).length > 3)) return [3 /*break*/, 2];
                res.status(403);
                res.send({ "message": "Error. Cannot take in other properties other than reason and amount." });
                return [2 /*return*/];
            case 2: return [4 /*yield*/, weddingService.retrieveWeddingById(id)];
            case 3:
                wedding = _a.sent();
                _a.label = 4;
            case 4: return [3 /*break*/, 6];
            case 5:
                error_5 = _a.sent();
                if (error_5 instanceof error_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_5);
                    return [2 /*return*/]; // We use return as to make sure we do not send multiple responses for our
                }
                return [3 /*break*/, 6];
            case 6:
                expense = req.body;
                if (!(Number(expense.amount) > 0)) return [3 /*break*/, 8];
                expense.wedding_id = id;
                return [4 /*yield*/, expenseService.registerExpense(expense)];
            case 7:
                expense = _a.sent();
                res.status(201);
                res.send(expense);
                return [3 /*break*/, 9];
            case 8:
                res.status(403);
                res.send({ "message": "Error. Amount must be positive." });
                _a.label = 9;
            case 9: return [2 /*return*/];
        }
    });
}); });
/*
Endpoint: GET expenses of a specific wedding
Description: Verifies that the wedding ID is valid, then makes a list with all expenses for every wedding and iterates through them
             choosing the ones that have a matching wedding_id with the URL id.  Returns an error message if the wedding does not have any
             expenses or the wedding ID is not valid.
*/
app.get("/weddings/:id/expenses", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, wedding, error_6, expenses, weddingExpenses, i;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = Number(req.params.id);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, weddingService.retrieveWeddingById(id)];
            case 2:
                wedding = _a.sent();
                return [3 /*break*/, 4];
            case 3:
                error_6 = _a.sent();
                if (error_6 instanceof error_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_6);
                    return [2 /*return*/];
                }
                return [3 /*break*/, 4];
            case 4: return [4 /*yield*/, expenseService.retrieveAllExpenses()];
            case 5:
                expenses = _a.sent();
                weddingExpenses = [];
                for (i = 0; i < expenses.length; i++) {
                    if (expenses[i].wedding_id === id) {
                        weddingExpenses.push(expenses[i]);
                    }
                }
                if (weddingExpenses.length === 0) // If the wedding has no expenses, we return a status 404 instead of an empty array
                 {
                    res.status(404);
                    res.send({ "message": "The wedding with wedding_id " + id + " has no expenses." });
                    return [2 /*return*/];
                }
                res.status(200);
                res.send(weddingExpenses);
                return [2 /*return*/];
        }
    });
}); });
/*
Endpoint: GET expense with specific ID
Description: Gets all expenses matching the specified URL ID.  Returns an error if the ID does not match
             any existing expenses.
*/
app.get("/expenses/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var expenseId, expense, error_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                expenseId = Number(req.params.id);
                return [4 /*yield*/, expenseService.retrieveExpenseById(expenseId)];
            case 1:
                expense = _a.sent();
                res.status(200);
                res.send(expense);
                return [3 /*break*/, 3];
            case 2:
                error_7 = _a.sent();
                if (error_7 instanceof error_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_7);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
/*
Endpoint: PUT updates the expense information (currently only reason and amount)
Description: Must specify the id, the reason, and the new amount of the account along with the associated wedding_id.
             Returns an error if more fields are given, and if the ID of the URL does not match any existing id values.
*/
app.put("/expenses/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, expense, error_8;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 5]);
                if (Object.keys(req.body).length > 5) {
                    throw new error_1.MissingResourceError("message: Cannot add properties other than id, reason, amount, wedding_id, and a photo.");
                }
                id = Number(req.params.id);
                expense = req.body;
                if (!(id === req.body.id)) return [3 /*break*/, 2];
                return [4 /*yield*/, expenseService.modifyExpense(expense)];
            case 1:
                expense = _a.sent();
                res.status(200);
                res.send(expense);
                return [2 /*return*/];
            case 2:
                res.status(403);
                res.send({ "message": "Error. ID of URL must match body ID." });
                _a.label = 3;
            case 3: return [3 /*break*/, 5];
            case 4:
                error_8 = _a.sent();
                if (error_8 instanceof error_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_8);
                }
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
/*
Endpoint: DELETE an expense
Description: Deletes a expense with a matching ID of the URL.  Returns an error if the id does not exist.
*/
app["delete"]("/expenses/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, expenseExists, error_9;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                id = Number(req.params.id);
                return [4 /*yield*/, expenseService.removeExpense(id)];
            case 1:
                expenseExists = _a.sent();
                res.status(205);
                res.send(expenseExists);
                return [3 /*break*/, 3];
            case 2:
                error_9 = _a.sent();
                if (error_9 instanceof error_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_9);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.listen(3001, function () {
    console.log("Application Started");
});
